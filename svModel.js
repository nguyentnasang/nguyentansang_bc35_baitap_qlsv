function SinhVien(
  _maSv,
  _tenSv,
  _email,
  _matKhau,
  _diemToan,
  _diemLy,
  _diemHoa
) {
  this.ma = _maSv;
  this.ten = _tenSv;
  this.email = _email;
  this.matKhau = _matKhau;
  this.diemToan = _diemToan;
  this.diemLy = _diemLy;
  this.diemHoa = _diemHoa;
  this.tinhDTB = function () {
    var dtb = (this.diemToan * 1 + this.diemLy * 1 + this.diemHoa * 1) / 3;
    return dtb.toFixed(1);
  };
}
