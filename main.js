var dssv = [];
var dataLocal = localStorage.getItem("DSSV");
if (dataLocal) {
  dataRow = JSON.parse(dataLocal);

  dssv = dataRow.map(function (item) {
    return new SinhVien(
      item.ma,
      item.ten,
      item.email,
      item.matKhau,
      item.diemToan,
      item.diemLy,
      item.diemHoa
    );
  });
  renderDSSV(dssv);
}
function saveLocalStorage() {
  var dataJSON = JSON.stringify(dssv);
  localStorage.setItem("DSSV", dataJSON);
}
function themSv() {
  var newSV = layThongTinTuForm();
  dssv.push(newSV);
  renderDSSV(dssv);
  saveLocalStorage();
  document.getElementById("formQLSV").reset();
}
function xoaSV(idMa) {
  var index = dssv.findIndex(function (sv) {
    return sv.ma == idMa;
  });
  console.log("index", index);
  if (index == -1) return;
  dssv.splice(index, 1);
  renderDSSV(dssv);
  saveLocalStorage();
}
function suaSV(idMa) {
  var index = dssv.findIndex(function (sv) {
    return sv.ma == idMa;
  });
  showThongTinLenForm(dssv[index]);
  document.getElementById("txtMaSV").disabled = true;
}
function capNhatSv() {
  var editNewSv = layThongTinTuForm();
  var index = dssv.findIndex(function (sv) {
    return sv.ma == editNewSv.ma;
  });
  dssv[index] = editNewSv;
  renderDSSV(dssv);
  saveLocalStorage();
  document.getElementById("txtMaSV").disabled = false;
  document.getElementById("formQLSV").reset();
}
